package org.greg.server;

public interface Clock {
    long now();
}

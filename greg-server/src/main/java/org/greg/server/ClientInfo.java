package org.greg.server;

public class ClientInfo {
	
	public byte[] cidBytes;
	public byte[] host;
	
	
	public ClientInfo(byte[] cidBytes, byte[] host) {
		this.cidBytes = cidBytes;
		this.host = host;
	}
}

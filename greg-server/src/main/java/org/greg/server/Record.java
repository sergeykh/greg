package org.greg.server;

import java.util.UUID;

public class Record {
    public UUID uuid;
    public long serverTimestamp;
    public long timestamp;
    public byte[] message;

    public Record(UUID uuid, long serverTimestamp, long timestamp, byte[] message) {
        this.uuid = uuid;
        this.serverTimestamp = serverTimestamp;
        this.timestamp = timestamp;
        this.message = message;
    }

    public Record absolutizeTime(long lateness) {
        timestamp = timestamp - lateness;
        return this;
    }
}

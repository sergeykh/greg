package org.greg.client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPOutputStream;

public class Greg {
    private final ConcurrentLinkedQueue<Record> records = new ConcurrentLinkedQueue<Record>();
    private final AtomicInteger numDropped = new AtomicInteger(0);
    // Don't use ConcurrentLinkedQueue.size() because it's O(n)
    private final AtomicInteger numRecords = new AtomicInteger(0);
    private final Configuration conf = Configuration.INSTANCE;

    private final UUID uuid = UUID.randomUUID();
    private final String hostname;

    private final Thread pushMessagesThread;
    private final Thread initCalibrationThread;

    private volatile boolean isSoftShutdownRequested = false;
    private volatile boolean isHardShutdownRequested = false;

    private final static Greg instance = new Greg();

    private Greg() {
        pushMessagesThread = new Thread("GregPushMessages") {
            public void run() {
                pushCurrentMessages();
            }
        };
        pushMessagesThread.setDaemon(true);
        pushMessagesThread.start();

        initCalibrationThread = new Thread("GregInitiateCalibration") {
            public void run() {
                initiateCalibration();
            }
        };

        initCalibrationThread.setDaemon(true);
        initCalibrationThread.start();

        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            throw new RuntimeException("Can't get localhost?", e);
        }
    }

    public static void log(String message) {
        instance.logIntern(message);
    }

    /**
     * Requests shutdown and awaits until either
     * 1) remaining message count reaches zero or
     * 2) timeout elapses.
     * Use this method to make sure that everything your application had to say
     * has been sent, before shutting down the application.
     *
     * Completion of this method DOES NOT prevent further usage of {@link #log(String)}.
     *
     * @param timeoutMs How long to wait before abandoning hope to push all messages
     * (if this much elapses, all bets are off as to what messages have been pushed)
     * @return approximate number of message that haven't been pushed
     * @throws InterruptedException if the thread running this method is interrupted
     */
    public static int shutdownAndAwait(long timeoutMs) throws InterruptedException {
        return instance.shutdownAndAwaitIntern(timeoutMs);
    }

    private void logIntern(String message) {
        if (numRecords.get() < conf.maxBufferedRecords) {
            numRecords.incrementAndGet();

            int prevNumDropped = numDropped.getAndSet(0);

            if (prevNumDropped > 0 && Trace.ENABLED) {
                Trace.writeLine("Stopped dropping messages, " + prevNumDropped + " were dropped");
            }

            records.offer(new Record(PreciseClock.INSTANCE.now(), message));
        } else {
            int newNumDropped = numDropped.incrementAndGet();
            if (newNumDropped == 0) {
                Trace.writeLine("Starting to drop messages because of full queue");
            } else if (newNumDropped % 100000 == 0) {
                if(Trace.ENABLED) Trace.writeLine(newNumDropped + " dropped in a row...");
            }
        }
    }

    private int shutdownAndAwaitIntern(long timeoutMs) throws InterruptedException {
        isSoftShutdownRequested = true;

        long t0 = System.currentTimeMillis();
        pushMessagesThread.join(timeoutMs);

        long elapsed = System.currentTimeMillis() - t0;
        if(elapsed < timeoutMs)
            initCalibrationThread.join(timeoutMs - elapsed);

        isHardShutdownRequested = true;
        // Now the two threads should shut down soon.
        // However we don't care much, as they're daemon threads anyway.

        return records.size();
    }

    private boolean shouldTerminate() {
        return isHardShutdownRequested || (isSoftShutdownRequested && records.isEmpty());
    }

    private void pushCurrentMessages() {
        while (true) {
            if (shouldTerminate())
                break;

            while (records.isEmpty()) {
                if(!sleep(2)) return ;
            }

            if(numRecords.get() < 50) {
                if(!sleep(50)) return ;
            }

            Socket client = null;
            OutputStream bStream = null;
            LittleEndianDataOutputStream stream = null;

            try {
                client = new Socket(conf.server, conf.port);

                if(Trace.ENABLED) Trace.writeLine("Client connected to " + client.getRemoteSocketAddress() +
                                " from " + client.getLocalSocketAddress());

                bStream = new BufferedOutputStream(client.getOutputStream(), 65536);

                DataOutput w = new LittleEndianDataOutputStream(bStream);
                w.writeLong(uuid.getLeastSignificantBits());
                w.writeLong(uuid.getMostSignificantBits());
                byte[] cidBytes = conf.clientId.getBytes("utf-8");
                w.writeInt(cidBytes.length);
                w.write(cidBytes);

                byte[] machineBytes = hostname.getBytes("utf-8");
                w.writeInt(machineBytes.length);
                w.write(machineBytes);

                w.writeBoolean(conf.useCompression);

                stream = new LittleEndianDataOutputStream(
                                new BufferedOutputStream(conf.useCompression ?
                                        new GZIPOutputStream(bStream, 512, true) : bStream, 32536)
                );

                writeRecordsBatchTo(stream);

            } catch (InterruptedIOException ioe) {
                return;
            } catch (Exception e) {
                Trace.writeLine("Failed to push messages: ", e);
                // Ignore: logging is not *that* important and we're not a persistent message queue.
                // Perhaps better luck during the next iteration.
            } finally {
                close(stream);
                close(bStream);
                close(client);
            }

            // Only sleep when waiting for new records.
//            if (re && !sleep(conf.flushPeriodMs)) {
//                return;
//            }
        }
    }

    private void close(Closeable c) {
        if (c != null) {
            try {
                c.close();
            } catch (IOException e) {
                // Ignore
            }
        }
    }

    private void close(Socket sock) {
        if (sock != null) {
            try {
                sock.close();
            } catch (IOException e) {
                // Ignore
            }
        }
    }

    private boolean sleep(long millis) {
        try {
            Thread.sleep(millis);
            return true;
        } catch (InterruptedException e) {
            return false;
        }
    }

    private void writeRecordsBatchTo(LittleEndianDataOutputStream w) throws IOException {
        final int maxBatchSize = 5000;

        byte[] msgBuf = new byte[1];

        while(!shouldTerminate()) {
            int recordsWritten = 0;

            for(Record rec : records) {
                w.writeLong(rec.timestamp);

                int maxLen = rec.message.length() * 3 + 1;

                if(maxLen > msgBuf.length) {
                    msgBuf = new byte[maxLen];
                }

                int bytesWritten = charsToUtfBytes(rec.message, msgBuf);

                w.writeInt(bytesWritten);
                w.write(msgBuf, 0, bytesWritten);

                if(++recordsWritten == maxBatchSize)
                    break;
            }

            w.writeLong(0);
            w.flush();

            // Only remove records once we're sure that they have been written to server
            // (no exception happened to this point)
            for(int i = 0; i < recordsWritten; ++i) {
                records.remove();
            }

            numRecords.addAndGet(-recordsWritten);

            Trace.writeLine("Written batch of " + recordsWritten + " records to greg");

            while (records.isEmpty()) {
                if(!sleep(2)) return ;
            }

            if(numRecords.get() < 50) {
                if(!sleep(50)) return ;
            }
        }
    }

    private void initiateCalibration() {
        final long calibrationPeriod = 1000L * conf.calibrationPeriodSec;
        while (!shouldTerminate()) {
            Socket client = null;

            try {
                client = new Socket(conf.server, conf.calibrationPort);

                if(Trace.ENABLED) Trace.writeLine("Calibration client connected to " +
                        client.getRemoteSocketAddress() +
                        " from " + client.getLocalSocketAddress());

                client.setTcpNoDelay(true);
                exchangeTicksOver(client.getInputStream(), client.getOutputStream());
            } catch (InterruptedIOException iex) {
                return ;
            } catch (Exception e) {
                Trace.writeLine("Failed to exchange clock ticks during calibration, ignoring" + e);
            } finally {
                close(client);
            }

           if(!sleep(calibrationPeriod)) return ;
        }
    }

    private void exchangeTicksOver(InputStream in, OutputStream out) throws IOException {
        DataOutput w = new LittleEndianDataOutputStream(out);
        DataInput r = new LittleEndianDataInputStream(in);

        w.writeLong(uuid.getLeastSignificantBits());
        w.writeLong(uuid.getMostSignificantBits());

        while (!shouldTerminate()) {
            // Here they measure their time and send it to us. It arrives after network latency.
            try {
                r.readLong(); // Their ticks
            } catch (EOFException e) {
                break;
            }
            w.writeLong(PreciseClock.INSTANCE.now());
            // Our sample arrives to them after network latency.
        }
    }

    /**
     * Converts characters array into UTF bytes array.
     */
    private static int charsToUtfBytes(String str, byte[] byteBuffer) {
        final int len = str.length();
        int pos = 0;

        for(int chPos = 0; chPos < len; chPos++) {
            int c = str.charAt(chPos);

            if ((c >= 0x0001) && (c <= 0x007F)) {
                byteBuffer[pos++] = (byte) c;
            } else if (c > 0x07FF) {
                byteBuffer[pos++] = ((byte) (0xE0 | ((c >> 12) & 0x0F)));
                byteBuffer[pos++] = ((byte) (0x80 | ((c >>  6) & 0x3F)));
                byteBuffer[pos++] = ((byte) (0x80 | (c         & 0x3F)));
            } else {
                byteBuffer[pos++] = ((byte) (0xC0 | ((c >>  6) & 0x1F)));
                byteBuffer[pos++] = ((byte) (0x80 | (c         & 0x3F)));
            }
        }

        return pos;
    }

    public static void main(String[] a) throws InterruptedException {

        final String str = "Message message messagemessagemessagemessageme" +
                            "ssagemessagemessagemess" +
                            "agemessagfffffffffffffemfdsfasdfsdafsdfasdessageEND";

        final CountDownLatch latch = new CountDownLatch(1);

        Runnable r = new Runnable() {
            public void run() {
                try {
                    for(int i = 0; i < 100000; i++) {
                        Greg.log(str);

                        /*if(i / 5000 == 0) {
                            Thread.sleep(500L);
                        }*/
                    }

                    latch.countDown();
                } catch (Exception e) {}
            }
        };

        new Thread(r).start();
        new Thread(r).start();
        new Thread(r).start();
        new Thread(r).start();

        latch.await();

        Greg.shutdownAndAwait(10000L);
    }
}

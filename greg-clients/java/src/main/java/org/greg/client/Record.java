package org.greg.client;

class Record {
    public long timestamp;
    public String message;

    public Record(long timestamp, String message) {
        this.timestamp = timestamp;
        this.message = message;
    }
}

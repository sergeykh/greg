package org.greg.client.log4j;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Layout;
import org.apache.log4j.spi.LoggingEvent;
import org.greg.client.Greg;

public class GregAppender extends AppenderSkeleton {
	
    private String server				= System.getProperty("greg.server", "localhost");
    private String port					= System.getProperty("greg.port", "5676");
    private String calibrationPort		= System.getProperty("greg.calibrationPort", "5677");
    private int flushPeriodMs			= Integer.parseInt(System.getProperty("greg.flushPeriodMs", "1000"));
    private String clientId				= System.getProperty("greg.clientId", "unknown");
    private int maxBufferedRecords		= Integer.parseInt(System.getProperty("greg.maxBufferedRecords", "1000000"));
    private boolean useCompression		= Boolean.parseBoolean(System.getProperty("greg.useCompression", "true"));
    private int calibrationPeriodSec	= Integer.parseInt(System.getProperty("greg.calibrationPeriodSec", "10"));

    public GregAppender() {}
    

	/**
	 * Returns Greg server address.
	 * @return the server
	 */
	public String getServer() {
		return server;
	}

	/**
	 * Sets Greg server address string.
	 * @param server the new server address.
	 */
	public void setServer(String server) {
		this.server = server;
	}

	/**
	 * Returns Greg server port.
	 * @return the port server port.
	 */
	public String getPort() {
		return port;
	}

	/**
	 * Sets new Greg server port.
	 * @param port the server port to set.
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * Returns server port which accepts calibration exchanges.
	 * @return the calibrationPort calibration port.
	 */
	public String getCalibrationPort() {
		return calibrationPort;
	}

	/**
	 * Sets server port which accepts calibration exchanges.
	 * @param calibrationPort the calibrationPort to set
	 */
	public void setCalibrationPort(String calibrationPort) {
		this.calibrationPort = calibrationPort;
	}

	/**
	 * Returns interval between flushing record batches to server 
	 * (but if there are more than 10,000 messages in a batch, 
	 * the batch will be cut into pieces of 10,000).
	 * 
	 * @return the flushPeriodMs
	 */
	public int getFlushPeriodMs() {
		return flushPeriodMs;
	}

	/**
	 * Sets  interval between flushing record batches to server 
	 * (but if there are more than 10,000 messages in a batch, 
	 * the batch will be cut into pieces of 10,000).
	 * 
	 * @param flushPeriodMs the flushPeriodMs in ms to set.
	 */
	public void setFlushPeriodMs(int flushPeriodMs) {
		this.flushPeriodMs = flushPeriodMs;
	}

	/**
	 * Returns client identifier. Corresponds to CLIENTID in the server output. 
	 * Usually a component's name, for example, "BasketService".
	 * @return the clientId
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * Sets client identifier. Corresponds to CLIENTID in the server output. 
	 * Usually a component's name, for example, "BasketService".
	 * @param clientId the clientId to set
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	/**
	 * Returns how many records to buffer; more will be dropped.
	 * @return the maxBufferedRecords how many records to buffer.
	 */
	public int getMaxBufferedRecords() {
		return maxBufferedRecords;
	}

	/**
	 * Sets how many records to buffer; more will be dropped.
	 * @param maxBufferedRecords how many records to buffer.
	 */
	public void setMaxBufferedRecords(int maxBufferedRecords) {
		this.maxBufferedRecords = maxBufferedRecords;
	}

	/**
	 * Returns true if record batches will be gzip-compressed.
	 * @return the useCompression true to enable compression.
	 */
	public boolean isUseCompression() {
		return useCompression;
	}

	/**
	 * Sets whether to gzip-compress record batches.
	 * @param useCompression the useCompression to set
	 */
	public void setUseCompression(boolean useCompression) {
		this.useCompression = useCompression;
	}

	/**
	 * Returns how often to initiate calibration cycle in seconds.
	 * @return the calibrationPeriodSec
	 */
	public int getCalibrationPeriodSec() {
		return calibrationPeriodSec;
	}

	/**
	 * Sets how often to initiate calibration cycle in seconds.
	 * @param calibrationPeriodSec the calibrationPeriodSec to set
	 */
	public void setCalibrationPeriodSec(int calibrationPeriodSec) {
		this.calibrationPeriodSec = calibrationPeriodSec;
	}

	@Override
	public void activateOptions() {
	    System.setProperty("greg.server", server);
	    System.setProperty("greg.port", port);
	    System.setProperty("greg.calibrationPort", calibrationPort);
	    System.setProperty("greg.flushPeriodMs", String.valueOf(flushPeriodMs));
	    System.setProperty("greg.clientId", clientId);
	    System.setProperty("greg.maxBufferedRecords", String.valueOf(maxBufferedRecords));
	    System.setProperty("greg.useCompression", String.valueOf(useCompression));
	    System.setProperty("greg.calibrationPeriodSec", String.valueOf(calibrationPeriodSec));
	}
	
    @Override
    protected void append(LoggingEvent loggingEvent) {
    	Layout layout = getLayout();
    	
    	StringBuilder message = new StringBuilder(layout==null ? loggingEvent.getRenderedMessage() : layout.format(loggingEvent));
    	if((layout!=null && layout.ignoresThrowable()) || (layout==null)) {
    		String[] s = loggingEvent.getThrowableStrRep();
    		if (s!=null) {
    			for (String string : s) {
    				message.append(string).append(Layout.LINE_SEP);
    			}
    		}
    	}
    	
    	Greg.log(message.toString());
    }

    public void close() {
    	try {
			Greg.shutdownAndAwait(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }

    public boolean requiresLayout() {
        return false;
    }
}
